from django.db import models

# Create your models here.
class Users(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    city = models.CharField(max_length=50)


class UserDetails(models.Model):
    user = models.ForeignKey(Users, related_name = 'details', on_delete=models.CASCADE)
    mobile = models.CharField(max_length=15)
    email = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'user_details'

"""
class UserDetails(models.Model):
    user_id = models.IntegerField()
    mobile = models.CharField(max_length=15)
    email = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'user_details'
"""