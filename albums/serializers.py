from rest_framework import serializers
from .models import Album, Tracks
from django.db import models

class AlbumSerializer(serializers.ModelSerializer):

    # tracks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # tracks = TracksSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        fields = ['album_name', 'artist']

class TracksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tracks
        fields = ['title', 'duration', 'status']

class RawAlbumSerializer(serializers.Serializer):
    album_name = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)

    class Meta:
        fields = ['album_name', 'artist']


# class TracksSerializer(serializers.ModelSerializer):
#     album = AlbumSerializer(many=True, read_only=True)
    
#     class Meta:
#         model = Tracks
#         fields = ['title', 'album', 'duration', 'status']


class AlbumTracksSerializer(serializers.ModelSerializer):
    tracks = TracksSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        fields = ['album_name', 'artist', 'tracks']


    # def create(self, validated_data):
    #     choice_validated_data = validated_data.pop('choice_set')
    #     question = Question.objects.create(**validated_data)
    #     choice_set_serializer = self.fields['choice_set']
    #     for each in choice_validated_data:
    #         each['question'] = question
    #     choices = choice_set_serializer.create(choice_validated_data)
    #     return question