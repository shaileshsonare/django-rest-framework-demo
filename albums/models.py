# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Album(models.Model):
    album_name = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'album'


class Tracks(models.Model):
    album = models.ForeignKey(Album, related_name = 'tracks', on_delete=models.DO_NOTHING)
    # album = models.ForeignKey(Album, related_name = 'tracks', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    duration = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tracks'
