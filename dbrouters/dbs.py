# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/multiple_databases.html
# https://www.protechtraining.com/blog/post/tutorial-using-djangos-multiple-database-support-477

class CloudmailDb:
    route_app_labels = 'cloudmaildemo_db_label_' #label name which you need to include in models
    db_conn = 'cloudmaildemo_db' #connection name which you've put in settings databases

    def db_for_read(self, model, **hints):
        if model._meta.app_label == self.route_app_labels:
            return self.db_conn
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == self.route_app_labels:
            return self.db_conn
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if (
            obj1._meta.app_label == self.route_app_labels or \
            obj2._meta.app_label == self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in self.route_app_labels:
            return db == self.db_conn
        return None




class CloudmailDbTest:
    """
    A router to control all database operations on models in the
    auth and contenttypes applications.
    """
    route_app_labels = {'user_data', 'cloudmaildemo_db_label'}

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth and contenttypes models go to cloudmaildemo.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'cloudmaildemo_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth and contenttypes models go to cloudmaildemo.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'cloudmaildemo_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth or contenttypes apps is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or \
            obj2._meta.app_label in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth and contenttypes apps only appear in the
        'cloudmaildemo' database.
        """
        if app_label in self.route_app_labels:
            return db == 'cloudmaildemo_db'
        return None

class TestDb:
    """
    A router to control all database operations on models in the
    auth and contenttypes applications.
    """
    route_app_labels = {'auth', 'contenttypes'}

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth and contenttypes models go to test.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'testdb'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth and contenttypes models go to cloudmaildemo.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'testdb'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth or contenttypes apps is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or
            obj2._meta.app_label in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth and contenttypes apps only appear in the
        'cloudmaildemo' database.
        """
        if app_label in self.route_app_labels:
            return db == 'testdb'
        return None
