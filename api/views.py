from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from rest_framework.decorators import api_view,action
from rest_framework.response import Response
from users.models import Users, UserDetails
from .serializers import UsersSerializer, UserDetailsSerializer
from django.middleware.csrf import rotate_token
import json
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from django_filters import FilterSet
from django_filters import rest_framework as filters


# class ColsFilter(FilterSet):
    
#     class Meta:
#         model = Users
#         fields = ['name', 'age']


from albums.models import Album;
from albums.serializers import AlbumSerializer, AlbumTracksSerializer, RawAlbumSerializer;

# Raw SQL [Start]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

from django.db import connection
@api_view(["GET"])
def getRawAlbum(request):
    cursor = connection.cursor()
    # bindparams = [0]
    # sql = "select a.*, t.* from album a left join tracks t ON a.id = t.album_id where t.status = %s"
    bindparams = {'status':1}
    sql = "select a.*, t.* from album a left join tracks t ON a.id = t.album_id where t.status = %(status)s"
    cursor.execute(sql, bindparams);
    data = dictfetchall(cursor)
    return Response(data);


# Raw SQL [Stop]

# https://www.django-rest-framework.org/api-guide/filtering/
# https://books.agiliq.com/projects/django-orm-cookbook/en/latest/or_query.html
# https://hacksoft.io/django-filter-chaining/
# https://docs.djangoproject.com/en/3.0/topics/db/queries/
# https://docs.djangoproject.com/en/3.0/topics/db/sql/
# https://stackoverflow.com/questions/21182725/django-rest-framework-filtering-against-query-param/21186065

class AlbumTracksViewSet(ModelViewSet):
    queryset = Album.objects.all() 
    serializer_class = AlbumTracksSerializer;

    def create(self, validated_data):
        # print(validated_data)
        params = self.request.data
        print(params);
        # obj = Album(album_name=params['album_name'], artist=params['artist'])
        # obj.save()
        serializer = AlbumTracksSerializer(data=params)
        serializer.is_valid()
        serializer.save()
        return Response({'status':'true'});

    def get_queryset(self):
        
        queryset = Album.objects.all();

        # albumname = self.request.query_params.get('album_name')
        # query_param = self.request.data;
        # print(query_param);

        # if query_param['album_name']:
        #     albumname = query_param['album_name']
        # if query_param['artist_name']:
        #     artistname = query_param['artist_name']

        # if albumname:
        #     queryset = queryset.filter(album_name__contains=albumname)

        # if artistname:
        #     queryset = queryset.filter(artist__contains=artistname)

        # filter on joined table
        # queryset = queryset.filter(tracks__title='deewana tera')

        # queryset = queryset[10:5]


        print(queryset.query)

        return queryset;


from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    page_query_param = 'page'
    max_page_size = 1000

    
# https://stackoverflow.com/questions/43197964/how-to-join-two-models-in-django-rest-framework
# https://www.django-rest-framework.org/api-guide/relations/#example
class UserDetailsViewSet(ModelViewSet):
    queryset = Users.objects.all() 
    serializer_class = UserDetailsSerializer;


# https://www.django-rest-framework.org/api-guide/routers/
class UserViewSet(ModelViewSet):
    serializer_class = UsersSerializer;
    queryset = Users.objects.all().only('name');
    lookup_field = 'id';
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    filter_fields = ('name',)
    # filter_class = ColsFilter
    ordering_fields = ('name','id',)
    search_fields = ('name',)
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        print(self.request.GET); #http://localhost:8000/api/users/users/?hello=world
        # reqdata = self.request.data; #{ "hello":"India" }
        # print(reqdata['hello']);
        queryset = Users.objects.all()
        # queryset = queryset.filter(name='anil',city='Mumbai')
        # print(self.request.get);
        # name = self.request.query_params.get('name', None)
        # if name is not None:
        #     queryset = queryset.filter(name=name)
        return queryset

    @action(methods=['get'], detail=True,
            url_path='get-users', url_name='get_users')
    def get_users(self, request, *args, **kwargs):
        return Response({'hello': 'world'});



# from django_rest_framework import Res

# Create your views here.
def index(request):
    return JsonResponse({"msg":"Hello API"})

def userList(request):
    # users = Users.objects.all();
    users = Users.objects.filter(id=2);
    print(users);
    return HttpResponse(users[0].city);

@api_view(["GET"])
def getUsers(request):
    # users = [
    #     {"name":"Shailesh", "age":31},
    #     {"name":"Priyanka", "age":29},
    # ]
    rotate_token(request)
    users = Users.objects.all();
    serialized = UsersSerializer(users, many=True)
    return Response(serialized.data)


@api_view(["POST"])
def getUsersPost(request):
    # users = [
    #     {"name":"Shailesh", "age":31},
    #     {"name":"Priyanka", "age":29},
    # ]
    # rotate_token(request)
    users = Users.objects.all();
    serialized = UsersSerializer(users, many=True)
    return Response(serialized.data)

@api_view(["GET", "POST"])
def getJsonParam(request):
    params = request.data;
    print(params);
    print(request.GET);
    return Response({'test':'testvalue', 'params': params['email']});

# https://corporate.netcore.co.in/gitbucket/merajuddin.khan/NetSuite365/blob/master/frontend/projects/Cloudmail/src/app/users/users.service.ts
# https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html
# https://www.google.com/search?q=login+using+jwt+token+angular+django+rest+framework&rlz=1C1SQJL_enIN830IN830&oq=login+using+jwt+token+angular+django+rest+framework&aqs=chrome..69i57j33.24497j0j4&sourceid=chrome&ie=UTF-8
# https://www.google.com/search?rlz=1C1SQJL_enIN830IN830&ei=SvofX8SdNI6P4-EP28OX4AY&q=put+false+authentiation+django+rest+framework+for+specific+api&oq=put+false+authentiation+django+rest+framework+for+specific+api&gs_lcp=CgZwc3ktYWIQAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwAzIHCAAQRxCwA1CE1T9YxPZAYPj6QGgDcAB4AIABAIgBAJIBAJgBAKABAaoBB2d3cy13aXrAAQE&sclient=psy-ab&ved=0ahUKEwjE6qKH2-_qAhWOxzgGHdvhBWwQ4dUDCAw&uact=5
# https://www.django-rest-framework.org/api-guide/authentication/
# https://django-rest-framework-simplejwt.readthedocs.io/en/latest/creating_tokens_manually.html
# http://localhost:8000/api/get-token/
# https://www.google.com/search?rlz=1C1SQJL_enIN830IN830&ei=awEgX-isNOaa4-EP3b26sAw&q=create+user+model+in+django+rest+framework&oq=create+user+model+in+django+rest+&gs_lcp=CgZwc3ktYWIQAzIICCEQFhAdEB4yCAghEBYQHRAeOgcIABBHELADOgYIABAWEB5QwVxY4GRgk3BoAXAAeACAAcwMiAGXEpIBCzAuMi4wLjEuOC0xmAEAoAEBqgEHZ3dzLXdpesABAQ&sclient=psy-ab&ved=0ahUKEwiojsDt4e_qAhVmzTgGHd2eDsYQ4dUDCAw&uact=5
# https://www.codingforentrepreneurs.com/blog/how-to-create-a-custom-django-user-model
# https://www.google.com/search?rlz=1C1SQJL_enIN830IN830&ei=0wEgX8qGEc7H4-EP2MWfmA0&q=create+simple+jwt+token+manually+django+rest+framework&oq=create+simple+jwt+token+manually+django+rest+framework&gs_lcp=CgZwc3ktYWIQAzIICCEQFhAdEB46BwgAEEcQsAM6BQgAEJECOgQIABBDOgcIABCxAxBDOgQILhBDOgUIABCxAzoICAAQsQMQgwE6AgguOgsILhCxAxDHARCjAjoFCC4QsQM6BAgAEAo6AggAOgYIABAWEB46BwghEAoQoAE6BAghEBU6BQghEKABUOrzQ1jg6URg4-tEaAlwAHgAgAGJA4gB716SAQkwLjI3LjI0LjaYAQCgAQGqAQdnd3Mtd2l6wAEB&sclient=psy-ab&ved=0ahUKEwiKvOie4u_qAhXO4zgGHdjiB9MQ4dUDCAw&uact=5
# https://medium.com/backticks-tildes/lets-build-an-api-with-django-rest-framework-part-2-cfb87e2c8a6c
# https://www.django-rest-framework.org/api-guide/authentication/
# https://www.geeksforgeeks.org/implement-token-authentication-using-django-rest-framework/
# https://stackoverflow.com/questions/59375706/django-rest-framework-avoid-authentication-jwt
# http://localhost:8000/api/user-list/

#https://medium.com/backticks-tildes/lets-build-an-api-with-django-rest-framework-part-2-cfb87e2c8a6c
from django.contrib.auth import authenticate, login
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
class SignInView(APIView):
    permission_classes = (AllowAny,);

    def get(self, request, format=None):
        user = authenticate(request, username="shailesh", password="%TGBb")
        print("Hello Sign in view");
        tokens = get_tokens_for_user(user);
        return Response({"msg":"Hello World", "tokens":tokens});
"""
@api_view(["GET"])
def getTokenManually(request):
    print("Get Token Manuall");
"""

from rest_framework_simplejwt.tokens import RefreshToken

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }




    """
    A simple ViewSet for viewing and editing accounts.
    
    queryset = Users.objects.all();
    serializer_class = UsersSerializer

    def get_queryset(self):
        return self.request.user.accounts.all()

    @action(methods=['get'], detail=True,
            url_path='get-users', url_name='get_users')
    def get_users(self, request, pk=None):
        return Response({'hello': 'world'});
    """
