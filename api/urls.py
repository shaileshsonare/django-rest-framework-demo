from django.urls import path,include;
from . import views

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


from rest_framework.routers import DefaultRouter, SimpleRouter

router = DefaultRouter();
router.register('users', views.UserViewSet);
router.register('user-details', views.UserDetailsViewSet)
router.register('album', views.AlbumTracksViewSet)

urlpatterns = router.urls

urlpatterns = [
    path('users/', include(router.urls), name="index"),
    path('albumraw/', views.getRawAlbum, name="albumraw"),
    # path('users/', views.getUsers, name="users"),
    # path('userspost/', views.getUsersPost, name="userspost"),
    # path('user-list/', views.userList, name="user-list"),
    # path('get-json-params/', views.getJsonParam, name="get-json-params"),
    # path('token/', TokenObtainPairView.as_view()),
    # path('token/refresh/', TokenRefreshView.as_view(), name="refresh-token"),
    # path('get-token/', views.SignInView.as_view(), name="gettoken"),
    # path('modelviews/', views.UserViewSet, name="model view set")
]