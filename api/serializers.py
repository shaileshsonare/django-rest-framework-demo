from rest_framework import serializers
from users.models import Users, UserDetails

class UsersSerializer(serializers.ModelSerializer):

    #to rename name to username
    username = serializers.CharField(source='name');

    class Meta:
        model = Users
        # fields = '__all__'
        fields = ('username',)

class DetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetails
        fields = ['email', 'mobile']

class UserDetailsSerializer(serializers.ModelSerializer):

    details = DetailsSerializer(many=True, read_only=True)

    class Meta:
        model = Users
        fields = ['id', 'name', 'details'];