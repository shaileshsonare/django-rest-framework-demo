from django.apps import AppConfig


class AlbumtracksConfig(AppConfig):
    name = 'albumtracks'
