from django.urls import path,include;
from . import views

from rest_framework.routers import DefaultRouter;

router = DefaultRouter();
router.register('albumlist', views.AlbumViewSet, basename='albumlist');
router.register('tracklist', views.TracksWithAlbumViewSet, basename='trackslist');
router.register('albumadd', views.AlbumTrackAddViewSet, basename='albumadd');

urlpatterns = [
    path('', views.index, name="index"),
    path('album/', include(router.urls), name="album"),
    path('createtracks/', views.TracksViewSet.as_view({'post':'create'}), name="index"),
]