from django.shortcuts import render
from rest_framework.decorators import api_view, action
from rest_framework.response import Response;
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import status
from .models import Album, Tracks



class TracksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tracks
        fields = '__all__'

class AlbumTracksSerializer(serializers.ModelSerializer):
    tracks = TracksSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        # fields = ['album_name', 'artist', 'tracks']
        fields = '__all__'

    def create(self, validated_data):
        print(self)
        print("Hello==================\n")
        print(validated_data)
        print("\nworld=================")

        # tracks = TracksSerializer(validated_data.get('tracks'))
        # serializer = AlbumTracksSerializer(data=request.data)
        tracksdata = {
                "id": 2,
                "title": "is kadar pyar hai ",
                "duration": 4,
                "status": 1
            }
        tracks = TracksSerializer(data=tracksdata, many=True)
        if tracks.is_valid():
            tracks.save()
        return Album.objects.create(**validated_data)

class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        # fields = ['album_name', 'artist']
        fields = '__all__'


class TracksWithAlbumSerializer(serializers.ModelSerializer):
    album = AlbumSerializer(read_only=True)

    class Meta:
        model = Tracks
        # fields = '__all__'
        # extra_fields = ['albumdetails']
        fields = ['id', 'title', 'album_id', 'album',];


# [18/08/2020 5:13 PM] Avinash Ghadshi: ============= SUBQUERY USING QUERYSET ===============
# queryset = CloudUserlg.objects.using('mail').filter(domid=CloudDomain.objects.using('mail').filter(name__iexact='nstest.com')[0])[0:5]

# [18/08/2020 5:14 PM] Avinash Ghadshi: class UserlgSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = CloudUserlg
#         fields = '__all__'
        
# class DomainSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = CloudDomain
#         fields = [ 'domid', ]


class TracksWithAlbumViewSet(viewsets.ModelViewSet):
    queryset = Tracks.objects.filter(id__gte=3)[:1]
    serializer_class = TracksWithAlbumSerializer;



@api_view(["GET"])
def index(request):
    queryset = Album.objects.all()
    print(queryset[0].tracks.all()[0].title)

    print(request.GET)
    print(request.data)
    data = queryset.values();
    serialized_data = AlbumTracksSerializer(queryset, many=True)
    return Response({'status':'true', 'data' : data, 'serialized_data':serialized_data.data})

class AlbumViewSet(viewsets.ModelViewSet):
    serializer_class = AlbumSerializer;

    def get_queryset(self):
        queryset = Album.objects.all()
        print(self.request.GET)
        print(self.request.data)
        return queryset;



# https://stackoverflow.com/questions/45607644/django-rest-framework-insert-multiple-objects-in-one-post-request
class TracksViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all() 
    serializer_class = TracksSerializer;

    def create(self, request, *args, **kwargs):
        data = request.data.get("tracks") if 'tracks' in request.data else request.data
        many = isinstance(data, list)
        print (data, many)
        serializer = TracksSerializer(data=data, many=many)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

#nested add


class TrackAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tracks
        fields = ['status', 'title', 'duration']

class AlbumAddSerializer(serializers.ModelSerializer):
    tracks = TrackAddSerializer(many=True)

    class Meta:
        model = Album;
        fields = ['album_name', 'artist', 'tracks']

    def create(self, validated_data):
        tracks_data = validated_data.pop('tracks')
        album = Album.objects.create(**validated_data)
        for track_data in tracks_data:
            Tracks.objects.create(album=album, **track_data)
        return album
    


class AlbumTrackAddViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all();
    serializer_class = AlbumTracksSerializer;

    def create(self, request, *args, **kwargs):
        print(request.GET)
        print(request.data)
        data = {
            "album_name": "The Grey Album",
            "artist": "Danger Mouse",
            "tracks": [
                {"status": 1, "title": "Public Service Announcement", "duration": 245},
                {"status": 2, "title": "What More Can I Say", "duration": 264},
                {"status": 3, "title": "Encore", "duration": 159},
            ],
        };
        print(data)
        serializer = AlbumAddSerializer(data=data)
        serializer.is_valid()
        serializer.save()
