from django.db import models

class Album(models.Model):
    album_name = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)

    class Meta:
        app_label = 'cloudmaildemo_db_label'
        managed = False
        db_table = 'album'


class Tracks(models.Model):
    # album = models.ForeignKey(Album, related_name= 'tracks', models.DO_NOTHING)
    album = models.ForeignKey(Album, related_name = 'tracks', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    duration = models.IntegerField()
    status = models.IntegerField()

    class Meta:
        app_label = 'cloudmaildemo_db_label'
        managed = False
        db_table = 'tracks'