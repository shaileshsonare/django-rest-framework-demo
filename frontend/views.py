from django.shortcuts import render
from django.middleware.csrf import rotate_token

# Create your views here.
def index(request):
    rotate_token(request)
    return render(request, 'home.html', {"name" : "Shailesh Sonare"})